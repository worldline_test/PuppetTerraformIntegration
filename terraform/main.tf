
provider "google"{
    project="nice-compass-326312"
    credentials = "${file("credentials.json")}"
    region="us-central1"
    zone="us-central1-a"
}



resource "google_compute_instance" "demo_instance" {
  name         = "demo-instance"
  machine_type = "n1-standard-1"
  zone         = "us-central1-a"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-10"
    }
  }

  network_interface {
    subnetwork =google_compute_subnetwork.example_subnet.id
    access_config {
    }
  }

  #  provisioner "remote-exec" {
  #   inline = [
  #     "sudo apt-get update",
  #     "sudo apt-get install -y puppet",
  #     "sudo puppet apply --modulepath=/etc/puppetlabs/code/environments/production/modules /etc/puppetlabs/code/environments/production/manifests/init.pp",
  #   ]

  #    connection {
  #     type        = "ssh"
  #     user        = "aayushanand804"
  #     private_key =  "${file("KEY")}"
  #     host        = google_compute_instance.demo_instance.network_interface.0.access_config.0.nat_ip
  #   }
  # }
}




resource "google_compute_network" "example_network" {
  name                    = "example-network"
  auto_create_subnetworks = false
}


resource "google_compute_subnetwork" "example_subnet" {
  name          = "example-subnet"
  ip_cidr_range = "10.0.1.0/24"
  region        = "us-central1"
  network       = google_compute_network.example_network.self_link
}


resource "google_compute_firewall" "allow_ssh" {
  name    = "allow-ssh"
  network = google_compute_network.example_network.name

  allow {
    protocol = "tcp"
    ports    = ["22"]
  }

  source_ranges = ["0.0.0.0/0"]
}