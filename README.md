
# Configuration Management with Puppet and Terraform

The objective of this assignment is to automate the configuration management of infrastructure resources provisioned using Terraform using Puppet.






## 
## Setup GitLab Repository with name PuppetTerraformIntegration:

![App Screenshot](https://github.com/AayushAnand07/Assignment_QuizApp/assets/41218074/c0839eac-6bc9-498b-9cac-04d633081d2d)


#
## Puppet configuration to setup nginx server in vm instance

![App Screenshot](https://github.com/AayushAnand07/Question-Generator/assets/41218074/9d10fc8a-c3c3-4418-a2d1-279a0905c321)

#

## Terraform plan to setup gcp vm instance

![App Screenshot](https://github.com/AayushAnand07/Question-Generator/assets/41218074/a80133a5-0a54-4920-b35a-636921a0fac1)

#

## Checking the live status of nginx server in vm instance through ssh

![App Screenshot](https://github.com/AayushAnand07/Question-Generator/assets/41218074/5a1584ce-f774-44cd-9e25-3468fd18cdd6)



#

## Nginx server started on vm instance ip

![App Screenshot](https://github.com/AayushAnand07/Question-Generator/assets/41218074/7d9dd189-ee29-4766-9f4b-95338cf46221)



